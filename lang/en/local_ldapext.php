<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     local_ldapext
 * @category    string
 * @copyright   2020 Wisdmlabs <info@wisdmlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Ldap Extention';
$string['ldapextsettings'] = 'Ldap Extention Settings';
$string['ldapexthelp'] = 'Ldap custom field mapping string';
$string['local'] = 'Local';
$string['external'] = 'External';
$string['none'] = 'None';
$string['fieldsync'] = '{$a->fieldname} Sync Option';
$string['fieldsync_help'] = 'Select option to from where you want to sync the data for custom field {$a->fieldname}
None - Do not change the field
Local - Sync locally generated data with specific pattern
External - Get data from external DB server based on the mapper data
';
$string['ext_dbhost'] = '{$a->fieldname} External DB Host';
$string['ext_dbhost_help'] = '{$a->fieldname} Add external DB host name from where you want data';
$string['ext_dbuser'] = '{$a->fieldname} External DB Username';
$string['ext_dbuser_help'] = '{$a->fieldname} External database username';
$string['ext_dbpass'] = '{$a->fieldname} External DB Password';
$string['ext_dbpass_help'] = '{$a->fieldname} External database password';
$string['ext_dbname'] = '{$a->fieldname} External DB Name';
$string['ext_dbname_help'] = '{$a->fieldname} External database name';
$string['customfield'] = '{$a->fieldname} Field';
$string['customfield_help'] = '{$a->fieldname} filed name which will be sync with the local custom field in case of local add pattern';
$string['fieldmapper'] = '{$a->fieldname} Field Mapper';
$string['fieldmapper_help'] = '{$a->fieldname} mapper field name which will be mapped by the external database ';
$string['fieldmappertext'] = '{$a->fieldname} Field Mapper Text';
$string['fieldmappertext_help'] = '{$a->fieldname} Field mapper text or selection text which field needs to be selected';
