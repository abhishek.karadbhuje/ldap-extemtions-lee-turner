<?php

require_once('../../../config.php');
require_once($CFG->libdir.'/moodlelib.php');

// $mdb = moodle_database::get_driver_instance('sqlsrv', 'native', true);
// $mdb->connect('10.10.0.51', 'EdwiserLMS', 'V@lrZs@plHlXgul5EgmNUu%rr', 'DEPARTMENTS', '');
// $record = $mdb->get_records('DEPARTMENT_STRUCTURE_JOB_TITLE ');
// echo "<pre>";
// var_dump($record);
// die;

$username = required_param('username', PARAM_ALPHA);
$password = required_param('password', PARAM_ALPHA);
$authplugin = get_auth_plugin('ldap');

// $newinfo = $authplugin->get_userinfo($username);
$extusername = core_text::convert($username, 'utf-8', $authplugin->config->ldapencoding);
$ldapconnection = $authplugin->ldap_connect();

$user_dn = $authplugin->ldap_find_userdn($ldapconnection, $extusername);

$search_attribs = array();
$attrmap = $authplugin->ldap_attributes();

foreach ($attrmap as $key => $values) {
    if (!is_array($values)) {
        $values = array($values);
    }
    foreach ($values as $value) {
        if (!in_array($value, $search_attribs)) {
            array_push($search_attribs, $value);
        }
    }
}

if (!$user_info_result = ldap_read($ldapconnection, $user_dn, '(objectClass=*)', $search_attribs)) {
    $authplugin->ldap_close();
    return false; // error!
}

$user_entry = ldap_get_entries_moodle($ldapconnection, $user_info_result);
if (empty($user_entry)) {
    $authplugin->ldap_close();
    return false; // entry not found
}

echo "<pre>";
print_r("[Data in AD Server]\n");
var_dump($user_entry);

$result = array();
foreach ($attrmap as $key => $values) {
    if (!is_array($values)) {
        $values = array($values);
    }
    $ldapval = NULL;
    foreach ($values as $value) {
        $entry = $user_entry[0];
        if (($value == 'dn') || ($value == 'distinguishedname')) {
            $result[$key] = $user_dn;
            continue;
        }
        if (!array_key_exists($value, $entry)) {
            continue; // wrong data mapping!
        }
        if (is_array($entry[$value])) {
            $newval = core_text::convert($entry[$value][0], $authplugin->config->ldapencoding, 'utf-8');
        } else {
            $newval = core_text::convert($entry[$value], $authplugin->config->ldapencoding, 'utf-8');
        }
        if (!empty($newval)) { // favour ldap entries that are set
            $ldapval = $newval;
        }
    }
    if (!is_null($ldapval)) {
        $result[$key] = $ldapval;
    }
}

$authplugin->ldap_close();

print_r("\n\n[Data in LMS]\n");
var_dump($result);
die;