<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin events page to define all events.
 *
 * @package     local_ldapext
 * @category    admin
 * @copyright   2020 Wisdmlabs <info@wisdmlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * List of observers
 * @var array
 */
$observers = array (
    /**
     * Event observer for role assignment
     */
    array (
        'eventname' => '\core\event\user_loggedin',
        'callback' => '\local_ldapext\observers::user_loggedin',
        'includefile' => '/local/ldapext/classes/observers.php'
    )
);