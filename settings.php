<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     local_ldapext
 * @category    admin
 * @copyright   2020 Wisdmlabs <info@wisdmlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$settings = new admin_settingpage('local_ldapext', get_string('ldapextsettings', 'local_ldapext'));

// Create 
$ADMIN->add( 'localplugins', $settings );

$PAGE->requires->js_call_amd('local_ldapext/ldapext', 'init');

$profilefields = $DB->get_records('user_info_field');
$dataoptions = array(
   'none' => get_string('none', 'local_ldapext'),
   'local' => get_string('local', 'local_ldapext'),
   'external' => get_string('external', 'local_ldapext')
);

// Prepare mapper options
if (!empty($profilefields)) {
   // Prepare mapper fields
   $mapperfields = array();
   array_map(function($value) use (&$mapperfields) {
      $mapperfields[$value->id] = $value->name;
   }, $profilefields);
}

foreach($profilefields as $fields) {
   // Add a setting field to the settings for this page
   $settings->add( new admin_setting_configselect(

      // This is the reference you will use to your configuration
      'local_ldapext/' . $fields->shortname . '_fieldsyncoption',

      // This is the friendly title for the config, which will be displayed
      get_string('fieldsync', 'local_ldapext', array('fieldname' => $fields->name)),

      // This is helper text for this config field
      get_string('fieldsync_help', 'local_ldapext', array('fieldname' => $fields->name)),

      // This is the default value
      'none',
      
      // Options for data sync
      $dataoptions
   ));

   $syncoption = get_config('local_ldapext', $fields->shortname . '_fieldsyncoption');

   if ($syncoption == 'external') {
      // Add a setting field to the settings for this page
      $settings->add( new admin_setting_configtext(

         // This is the reference you will use to your configuration
         'local_ldapext/' . $fields->shortname . '_dbhost',

         // This is the friendly title for the config, which will be displayed
         get_string('ext_dbhost', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is helper text for this config field
         get_string('ext_dbhost_help', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is the default value
         null,

         // This is the type of Parameter this config is
         PARAM_TEXT
      ));

      // Add a setting field to the settings for this page
      $settings->add( new admin_setting_configtext(

         // This is the reference you will use to your configuration
         'local_ldapext/' . $fields->shortname . '_dbuser',

         // This is the friendly title for the config, which will be displayed
         get_string('ext_dbuser', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is helper text for this config field
         get_string('ext_dbuser_help', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is the default value
         null,

         // This is the type of Parameter this config is
         PARAM_TEXT
      ));

      // Add a setting field to the settings for this page
      $settings->add( new admin_setting_configtext(

         // This is the reference you will use to your configuration
         'local_ldapext/' . $fields->shortname . '_dbpass',

         // This is the friendly title for the config, which will be displayed
         get_string('ext_dbpass', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is helper text for this config field
         get_string('ext_dbpass_help', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is the default value
         null,

         // This is the type of Parameter this config is
         PARAM_TEXT
      ));

      // Add a setting field to the settings for this page
      $settings->add( new admin_setting_configtext(

         // This is the reference you will use to your configuration
         'local_ldapext/' . $fields->shortname . '_dbname',

         // This is the friendly title for the config, which will be displayed
         get_string('ext_dbname', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is helper text for this config field
         get_string('ext_dbname_help', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is the default value
         null,

         // This is the type of Parameter this config is
         PARAM_TEXT
      ));

      $mapperoptions = array(
         'none' => get_string('none', 'local_ldapext')
      );
      $mapperoptions = $mapperoptions + $mapperfields;
      unset($mapperoptions[$fields->id]);
      // Add a setting field to the settings for this page
      $settings->add( new admin_setting_configselect(

         // This is the reference you will use to your configuration
         'local_ldapext/' . $fields->shortname . '_fieldmapper',

         // This is the friendly title for the config, which will be displayed
         get_string('fieldmapper', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is helper text for this config field
         get_string('fieldmapper_help', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is the default value
         'none',
         
         // Options for data sync
         $mapperoptions
      ));

      // Add a setting field to the settings for this page
      $settings->add( new admin_setting_configtext(

         // This is the reference you will use to your configuration
         'local_ldapext/' . $fields->shortname . '_fieldmappertext',

         // This is the friendly title for the config, which will be displayed
         get_string('fieldmappertext', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is helper text for this config field
         get_string('fieldmappertext_help', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is the default value
         'CN=.*',

         // This is the type of Parameter this config is
         PARAM_TEXT
      ));
   }

   if ($syncoption == 'local' || $syncoption == 'external') {
      // Add a setting field to the settings for this page
      $settings->add( new admin_setting_configtext(

         // This is the reference you will use to your configuration
         'local_ldapext/' . $fields->shortname . '_fieldname',

         // This is the friendly title for the config, which will be displayed
         get_string('customfield', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is helper text for this config field
         get_string('customfield_help', 'local_ldapext', array('fieldname' => $fields->name)),

         // This is the default value
         'CN=.*',

         // This is the type of Parameter this config is
         PARAM_TEXT
      ));
   }
}
