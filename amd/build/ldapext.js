define(['jquery'], function($) {
    // Init function
    var init = function() {
        window.onbeforeunload = null;
        $(document).on('change', 'select[id$="_fieldsyncoption"]', function() {
            $(this).closest('form').submit();
        });
    }

    // Return init function
    return {
        init: init
    }
});