<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin event observers.
 *
 * @package     local_ldapext
 * @category    admin
 * @copyright   2020 Wisdmlabs <info@wisdmlabs.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_ldapext;

defined('MOODLE_INTERNAL') || die();

use core_user;

/**
 * Course progress event observer
 */
class observers {
    /**
     * Observer for user loggedin event
     * @param  \core\event\role_assigned $event Event Data
     */
    public static function user_loggedin(\core\event\user_loggedin $event) {
        // Get event related data
        $eventdata = (object) $event->get_data();

        self::update_custom_profile_fields($eventdata->userid);
    }

    /**
     * Update custom profile fields
     */
    private static function update_custom_profile_fields($userid) {
        global $DB;
        $profilefields = $DB->get_records('user_info_field');

        if (empty($profilefields)) {
            return false;
        }

        foreach($profilefields as $field) {
            $option = get_config('local_ldapext', $field->shortname . '_fieldsyncoption');

            if (!$option || $option == 'none') {
                continue;
            }

            $params = array('userid' => $userid , 'fieldid' => $field->id);
            if ($option == 'local') {
                $patternstr = trim(get_config('local_ldapext', $field->shortname . '_fieldname'));
                if (!$patternstr && $patternstr != '') {
                    continue;
                }

                $pattern = "/" . $patternstr . ",.*/i";

                $fielddata = $DB->get_record('user_info_data', $params);
                
                if (!$fielddata || !preg_match($pattern, $fielddata->data)) {
                    continue;
                }

                $data = explode(',', $fielddata->data);
                foreach($data as $d) {
                    $newdata = explode('=', $d);
                    if (!isset($newdata[0]) && !isset($newdata[1])) {
                        continue;
                    }

                    $fieldname = str_replace('=.*', '', $patternstr);
                    if ($fieldname !== $newdata[0]) {
                        continue;
                    }

                    $DB->set_field('user_info_data', 'data', $newdata[1], $params);
                }
            } else if ($option == 'external') {
                $fieldname = trim(get_config('local_ldapext', $field->shortname . '_fieldname'));
                $extdbhost = trim(get_config('local_ldapext', $field->shortname . '_dbhost'));
                $extdbuser = trim(get_config('local_ldapext', $field->shortname . '_dbuser'));
                $extdbpass = trim(get_config('local_ldapext', $field->shortname . '_dbpass'));
                $extdbname = trim(get_config('local_ldapext', $field->shortname . '_dbname'));
                $dbdata = explode('.', $extdbname);
                if (!isset($dbdata[0]) || !isset($dbdata[1])) {
                    continue;
                }
                $dbname = $dbdata[0];
                $dbtable = $dbdata[1];

                $extdbfieldmapper = trim(get_config('local_ldapext', $field->shortname . '_fieldmapper'));
                $extdbfieldmappertext = trim(get_config('local_ldapext', $field->shortname . '_fieldmappertext'));

                try {
                    $mdb = \moodle_database::get_driver_instance('mysqli', 'native', true);
                    $test = $mdb->connect($extdbhost, $extdbuser, $extdbpass, $dbname, '');

                    $mapperfieldval = $DB->get_record('user_info_data', array('userid' => $userid, 'fieldid' => $extdbfieldmapper));
                    if (!$mapperfieldval || $mapperfieldval->data == "") {
                        continue;
                    }
                    
                    $extparam = array(
                        $extdbfieldmappertext => $mapperfieldval->data
                    );
                    $record = $mdb->get_record($dbtable, $extparam);

                    if (!$record) {
                        continue;
                    }

                    if (!isset($record->$fieldname)) {
                        continue;
                    }

                    if (!$extdbfieldmapper || $extdbfieldmapper == "") {
                        continue;
                    }

                    if (!isset($record->$extdbfieldmappertext)) {
                        continue;
                    }

                    $DB->set_field('user_info_data', 'data', $record->$fieldname, $params);
                } catch (\Throwable $th) {
                    // Do nothing
                }
            }
        }
    }
}